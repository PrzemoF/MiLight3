#!/usr/bin/env python
from __future__ import print_function
import argparse
import milightbox
import sys
import time


def main():
    parser = argparse.ArgumentParser(description='Mi-Light switch.')
    parser.add_argument('-o', '--on', action='store_true', default=False, help='Switch on')
    parser.add_argument('-f', '--off', action='store_true', default=False, help='Switch off')
    parser.add_argument('-z', '--zone', action='store', type=int, choices={1, 2, 3, 4}, help='Zone number', required=True)
    parser.add_argument('-t', '--time', action='store', type=int, help='Fade time in seconds, minimum 10 s')
    parser.add_argument('-k', '--kelvin', action='store', type=int, help='Light temperature, 2700 to 6500 [K]')
    parser.add_argument('-b', '--brightness', action='store', type=int, help='Light brightness, 0 to 100')
    parser.add_argument('-u', '--hue', action='store', type=int, help='Light hue, 0 to 100')
    parser.add_argument('-s', '--saturation', action='store', type=int, help='Light saturation, 0 to 100')

    arg = parser.parse_args()

    if not (arg.on or arg.off or arg.kelvin or arg.brightness or arg.hue or arg.saturation):
        parser.error("Please provide at least one option: --on, --off, --kelvin, --brightness, --hue or --saturation")

    if arg.kelvin and (arg.brightness or arg.hue or arg.saturation):
        parser.error("Using --kelvin option excludes --brightness, --hue and --saturation")

    if arg.time and arg.time < 10:
        parser.error("Minimum time is 10")

    if arg.kelvin and (arg.kelvin not in range(2700, 6501)):
        parser.error("Light temperature has to be between 2700 and 6500 [K]")

    if arg.brightness and (arg.brightness not in range(0, 101)):
        parser.error("Light brightness has to be between 0 and 100 [%]")

    if arg.hue and (arg.hue not in range(0, 256)):
        parser.error("Light hue has to be between 0 and 255")

    if arg.saturation and (arg.saturation not in range(0, 101)):
        parser.error("Light saturation has to be between 0 and 100 [%]")

    if len(sys.argv) < 2:
        parser.print_usage()
        sys.exit(1)

    milight = milightbox.ml_controller()
    time.sleep(0.1)

    if arg.on:
        milight.switch_on(arg.zone)
    if arg.kelvin is not None:
        milight.set_temperature_in_kelvin(arg.zone, arg.kelvin)
    if arg.brightness is not None:
        milight.set_brightness(arg.zone, arg.brightness)
    if arg.hue is not None:
        milight.set_hue(arg.zone, arg.hue)
    if arg.saturation is not None:
        milight.set_saturation(arg.zone, arg.saturation)
    if arg.on is not None and arg.time is not None:
        time_step = arg.time / 100.0
        for i in range(0, 100):
            milight.set_brightness(arg.zone, i)
            time.sleep(time_step)
    if arg.off:
        milight.switch_off(arg.zone)


if __name__ == "__main__":
    main()
