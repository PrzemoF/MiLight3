#!/usr/bin/python
import socket
import time

#Change IP to match your gateway
IP = "192.168.0.222"


UDP_PORT_RECEIVE = 55054
UDP_TIMES_TO_SEND_COMMAND = 1
SLEEP_TIME = 0.02


class ml_controller(object):
    #FIXME Make proper functions definitions
    HEADER = "31"
    PASS = "00 00"
    DEST = "08"
    TAIL = "00 00 00"

    def __init__(self, ip=IP, port=5987):
        #print ("__init__ of ml_controller called")
        self.ip = ip
        self.port = port
        self.seq = 0
        self.connect()
        self.zone = {}
        for n in range(1, 5):
            self.zone[n] = dict(hue=-1, saturation=100, brightness=100, temperature=0)

    def connect(self):
        #print ("connect of milight3 called")
        MESSAGE = "20 00 00 00 16 02 62 3A D5 ED A3 01 AE 08 2D 46 61 41 A7 F6 DC AF D3 E6 00 00 1E"
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('', UDP_PORT_RECEIVE))
        s.sendto(bytearray.fromhex(MESSAGE), (self.ip, self.port))
        s.close()

        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('', UDP_PORT_RECEIVE))

        data, addr = s.recvfrom(65536)
        #print("received message: " + data.encode('hex'))
        s.close()
        response = str(data.encode('hex'))
        ID1 = response[38:40]
        ID2 = response[40:42]
        #print("Session ID bytes: {} {}".format(ID1, ID2))

        self.session_header = "80 00 00 00 11 " + ID1 + " " + ID2

    def get_header(self):
        return self.session_header

    def get_hex_command(self, command):
        #format of {command} 9 byte packet =
        #0x31 {PasswordByte1 default 00} {PasswordByte2 default 00}
        #{remoteStyle 08 for RGBW/WW/CW or 00 for bridge lamp}
        #{LightCommandByte1} {LightCommandByte2}
        #0x00 0x00 0x00
        #{Zone1-4 0=All}
        #0x00
        #{Checksum}

        #31 00 00 08 04 01 00 00 00 = Light ON
        #31 00 00 08 04 02 00 00 00 = Light OFF
        #31 00 00 08 04 05 00 00 00 = Night Light ON
        #31 00 00 08 05 64 00 00 00 = White Light ON (Color RGB OFF)
        #31 00 00 08 01 BA BA BA BA = Set Color to Blue (0xBA)
        #(0xFF = Red, D9 = Lavender, BA = Blue, 85 = Aqua, 7A = Green, 54 = Lime, 3B = Yellow, 1E = Orange)
        #31 00 00 08 02 SS 00 00 00 = Saturation (SS hex values 0x00 to 0x64 : examples: 00 = 0%, 19 = 25%, 32 = 50%, 4B, = 75%, 64 = 100%)
        #31 00 00 08 03 BN 00 00 00 = BrightNess (BN hex values 0x00 to 0x64 : examples: 00 = 0%, 19 = 25%, 32 = 50%, 4B, = 75%, 64 = 100%)
        #31 00 00 08 05 KV 00 00 00 = Kelvin (KV hex values 0x00 to 0x64 : examples: 00 = 2700K (Warm White), 19 = 3650K, 32 = 4600K, 4B, = 5550K, 64 = 6500K (Cool White))
        conversions = {'on': '04 01',
                       'off': '04 02',
                       'night': '04 05',
                       'set colour': '01',
                       'saturation': '02',
                       'brightness': '03',
                       'kelvin': '05'}
        return conversions[command]

    def send(self, zone, command, value=0):
        #print ('zone: {}, command: {}, value: {}'.format(zone, command, value))
        cm_hex = self.get_hex_command(command)
        z_hex = '{:02X}'.format(zone)
        v_hex = '{:02X}'.format(value)
        s_hex = '{:02X}'.format(self.seq)
        if len(cm_hex) == 2:
            cm_hex = cm_hex + ' ' + v_hex
        if command == 'set colour':
            tail = v_hex + ' ' + v_hex + ' ' + v_hex
        else:
            tail = self.TAIL
        message = self.HEADER + ' ' + self.PASS + ' ' + self.DEST + ' ' + cm_hex + ' ' + tail + ' ' + z_hex
        checksum = 0
        for n in bytearray.fromhex(message):
            checksum += n
        message = message + ' 00 ' + format(checksum, "04X")[2:]
        #print message
        h = self.get_header()
        #message = h.replace("SN", format(self.seq, "02X")) + message
        message = h + ' 00 ' + s_hex + ' 00 ' + message
        #+ " 00 SN 00 "
        #print message
        self.send_raw(message)
        self.seq += 1
        if (self.seq >= 255):
            self.seq = 0
        time.sleep(SLEEP_TIME)

    def send_raw(self, command):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('', UDP_PORT_RECEIVE))
        for x in range(0, UDP_TIMES_TO_SEND_COMMAND):
            s.sendto(bytearray.fromhex(command), (self.ip, self.port))
        s.close()

    def raw_on(self, zone):
        self.send(zone, 'on')

    def raw_off(self, zone):
        self.send(zone, 'off')

    def set_hue(self, zone, val=None):
        if val is None:
            val = self.zone[zone]['hue']
        if val in range(-1, 256):
            self.send(zone, 'set colour', val)

    def set_saturation(self, zone, val=None):
        if val is None:
            val = self.zone[zone]['saturation']
        if val in range(0, 101):
            self.send(zone, 'saturation', 100 - val)

    def set_brightness(self, zone, val=None):
        if val is None:
            val = self.zone[zone]['brightness']
        if val in range(0, 101):
            self.send(zone, 'brightness', val)

    def set_temperature(self, zone, val=None):
        if val is None:
            val = self.zone[zone]['temperature']
        if val in range(0, 101):
            self.send(zone, 'kelvin', val)

    def set_temperature_in_kelvin(self, zone, val=None):
        if val is None:
            val = self.zone[zone]['temperature']
        k = int(100.0 * ((val - 2700.0) / 3800.0))
        if k in range(0, 101):
            self.send(zone, 'kelvin', k)

    def set_hsb(self, zone, hue, sat, bri):
        self.set_hue(zone, hue)
        self.set_saturation(zone, sat)
        self.set_brightness(zone, bri)

    def switch_on(self, zone, brightness=None):
        self.raw_on(zone)

    def switch_off(self, zone):
        self.raw_off(zone)

    def set_night(self):
        self.send('night')

    def set_white(self):
        self.raw_on()
        self.send('kelvin', 100)
