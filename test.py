#!/usr/bin/env python

import milightbox
import time

span = 5
step = span / 100.0
zone = 3
l = milightbox.ml_controller("192.168.0.222")
time.sleep(step)
l.send(3, 'on')
l.send(3, 'brightness', 0)
time.sleep(step)
for i in range(0, 100):
    l.send(3, 'brightness', i)
    l.send(3, 'kelvin', i)
    time.sleep(step)
l.send(3, 'brightness', 0)
l.switch_off(zone)
