# MiLight3

This code started as a fork of https://github.com/LumKitty/MiLight3, but now is far away from the original.

Python 2.7 control class for controlling MiLight-3.0 (Limitless V6.0) lights.  

This is work in progress, so not everythng works.

Setup: change IP in milightbox.py to match your gateway:

#Change IP to match your gateway
IP = "192.168.0.222"

Example usage from command line:

Switch on zone 3:
```bash
./switch.py --on --zone 3
```
Set light temperature to 2700 [[K]] (warm) and switch off zone 1, so it will be warm white on next switch on:
```bash
./switch.py --off -z 1 --kelvin 2700
```
Change hue to 255, saturation to 100 and brightness to 50:
```bash
./switch.py --hue 255 --saturation 100 --brightness 50 --zone 3
```

Usage from python:
```python
>>> import milightbox
>>> l = milightbox.ml_controller()
```
or
```python
>>> l = milightbox.ml_controller("192.168.0.222")
```
Switch on zone 3:
```python
>>> l.switch_on(3)
```
Set light temperature to 2700 [K] (warm) and switch off zone 1, so it will be warm white on next switch on:
python
```python
>>> l.set_temperature_in_kelvin(3, 2700)
>>> l.switch_off(3)
```
Change hue to 255, saturation to 100 and brightness to 50:
```pyton
>>> l.set_hue(3, 255)
>>> l.set_saturation(3, 100)
>>> l.set_brightness(3, 50)
```
Full list of user commands:
```python
set_hue(zone, val)
set_saturation(zone, val)
set_brightness(zone, val)
set_temperature(zone, val)
set_temperature_in_kelvin(zone, val)
set_hsb(zone, hue, sat, bri)
switch_on(zone, brightness)
switch_off(zone)
set_night()
set_white()
```
